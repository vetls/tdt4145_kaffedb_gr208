#from msilib.schema import Error
import sqlite3


# General Functions to access database
def DatabasePost(info, sql):
    conn = sqlite3.connect('core/coffeeDB.db')
    cur = conn.cursor()
    cur.execute(sql, info)
    conn.commit()
    val =  cur.lastrowid
    conn.close()
    return val

def DatabaseGetWihtOutInfo(sql):
    conn = sqlite3.connect('core/coffeeDB.db')
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    rows = cur.fetchall()
    conn.close()
    return rows

def DatabaseGet(sql, info):
    conn = sqlite3.connect('core/coffeeDB.db')
    cur = conn.cursor()
    cur.execute(sql, info)
    conn.commit()
    rows =  cur.fetchall()
    conn.close()
    return rows
    