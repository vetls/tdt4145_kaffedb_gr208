SELECT DISTINCT Coffee.Name, Coffee.RoasteryName 
FROM Coffee 
LEFT OUTER JOIN Review ON (Coffee.CoffeeID = Review.CoffeeID)
INNER JOIN Batch ON(Coffee.BatchID = Batch.BatchID)
INNER JOIN Farm ON (Batch.FarmName = Farm.Name) 
INNER JOIN InBatch ON (Batch.BatchID = InBatch.BatchID) 
WHERE Farm.Country = 'El Salvador'

 
