from datetime import date
import datetime
from itertools import count
import DBHandler
#from msilib.schema import Error
import sqlite3


def getNextReviewID():

    sql = "SELECT * FROM Review"
    rows = DBHandler.DatabaseGetWihtOutInfo(sql)
    return len(rows) + 1

def getNextCoffeeID():
    sql = "SELECT * FROM Coffee"
    rows = DBHandler.DatabaseGetWihtOutInfo(sql)
    return len(rows) + 1    


def getNextBatchID():
    sql = "SELECT * FROM Batch"
    rows = DBHandler.DatabaseGetWihtOutInfo(sql)
    return len(rows) + 1  

# Classes for all used tables in DB. Own Post and Get functions with their unique SQL formats.
class User:
    def __init__(self, email, fname, lname, password):
        self.Email = email
        self.FName = fname
        self.LName = lname
        self.Password = password

    def postUser(self):
        sql = '''INSERT INTO User
                VALUES(?,?,?,?)'''
        user = (self.Email, self.FName, self.LName, self.Password)

        DBHandler.DatabasePost(user,sql)

    def getUser(self):
        sql = '''SELECT * FROM User'''

        DBHandler.DatabaseGet(sql)


class Review:
    def __init__(self, email, score, tastenote, coffeeID):
        self.ReviewID = getNextReviewID()
        self.CoffeeID = coffeeID
        self.Date = date.today()
        self.Email = email
        self.Score = score
        self.TasteNote = tastenote
       

    def postReview(self):
        sql = '''INSERT INTO Review
                VALUES(?,?,?,?,?,?)'''
        review = (self.ReviewID, self.CoffeeID, self.Score, self.TasteNote, self.Date, self.Email)

        DBHandler.DatabasePost(review,sql)

    def getReview(self):
        sql = '''SELECT * FROM Review'''

        DBHandler.DatabaseGet(sql)


def verifyAccount(email, password):
    
    """
    Checks wether email exists in database and if password matches user. 
    @param email: email of user to log in
    @param passord: Password of user to log in
    @returns: True if email exists and password is correct. Else returns False. 
    """

    conn = sqlite3.connect('core/coffeeDB.db')
    cur = conn.cursor()
    sql = "SELECT * FROM User WHERE (User.Email =? AND User.Password =?)"
    info = (email, password)
    cur.execute(sql, info)
    account = cur.fetchall()
    if not account:
        return False
    return True



def logIn (email, password):

    """
    Checks wether emails ecist and password is correct.
    @param email: email of user to log in
    @param passord: Password of user to log in
    @returns: user-Object if user exists. else returns None
    """
    if (not verifyAccount(email, password)):
        print("Account does not exist")
        return None
    else:
        sql = "SELECT * FROM User WHERE Email=?"
        info = (email,)
        userRow = DBHandler.DatabaseGet(sql, info)
        user = User(userRow[0][0], userRow[0][1], userRow[0][2], userRow[0][3])
        print("login successfull")
        return user
    

def getAllCoffees():
    sql = "SELECT Name, RoasteryName FROM Coffee"
    rows = DBHandler.DatabaseGetWihtOutInfo(sql)
    return rows

def getReviewsForCoffee(coffee_name, roastery):
    
    """
    @param coffee_name: Name of coffee
    @param roastery: name of roastery that produced coffee. 
    @returns: list of all reviews about entered coffee if it exists. Else returns None
    """

    coffeeID = getCoffeeIDByNameAndRoastery(coffee_name, roastery)
    if coffeeID == None:
        return None
    sql = "SELECT * FROM Review WHERE CoffeeID =?"
    info = (coffeeID,)
    reviews = DBHandler.DatabaseGet(sql, info)
    return reviews
    
def getCoffeeIDByNameAndRoastery(name, roastery):

    """
    @param coffee_name: Name of coffee
    @param roastery: name of roastery that produced coffee.
    @returns: CoffeeID of entered coffee if it exists. Else returns None.
    """
    
    sql = "SELECT CoffeeID FROM Coffee WHERE (Coffee.Name = ?) and (RoasteryName = ?)"
    info = (name, roastery)
    coffee = DBHandler.DatabaseGet(sql, info)
    if not coffee:
        return None
    
    return coffee[0][0]

def getCurrentYear():
    currentDateTime = datetime.datetime.now()
    date = currentDateTime.date()
    year = date.strftime("%Y")
    return year

def getMostActiveUsersThisYear():

    """
    @returns: list of users sorted by amount of reviews. Most active user first. 
    """

    sql = '''SELECT FName, LName, COUNT(DISTINCT CoffeeID) as tasted FROM Review NATURAL JOIN User 
        WHERE strftime('%Y', Review.Date) = ? GROUP BY User.Email ORDER BY tasted DESC'''
    info = (getCurrentYear(),)
    rows = DBHandler.DatabaseGet(sql, info)
    return rows

def getCoffeeByCriteria():

    """
    Recieves input criteria from client. Recieves wich countries, descriptions, 
    processingmethods or beans to include or exclude. 
    @returns: list of coffees and roasteries matching given criteria. 
    """

    #Recives all paramteters to include in query.
    print("Find specific coffees using keywords. Seperate with ', ' You can leave a field blank if you do not wish to enter criteria\n")
    countries_string = input("Origin countries to include: ").lower()
    tn_string = input("Description keywords to include: ").lower()
    pm_string = input("Processing methods to include : ")
    beans_string = input("Coffee beans to include: ").lower()

    countries = countries_string.split(", ")
    pm_incl = pm_string.split(", ")
    tn = tn_string.split(", ")
    beans = beans_string.split(", ")


    #Recieves paramaters to exclude from query
    print("Enter coffee criteria you do not want. Seperate with ', ' You can leave a field blank if you do not wish to enter exclusion-criteria\n")
    countries_excl_string = input("Origin countries to exclude: ").lower()
    processing_method_excl = input("processing methods to exclude : ").lower()
    beans_excl_string = input("Coffee beans to exclude: ").lower()

    countries_excl = countries_excl_string.split(", ")
    pm_excl = processing_method_excl.split(", ")
    beans_excl = beans_excl_string.split(", ")

    #Base sql without criteria
    sql = '''SELECT DISTINCT Coffee.Name, Coffee.RoasteryName 
    FROM Coffee 
    LEFT OUTER JOIN Review ON (Coffee.CoffeeID = Review.CoffeeID)
    INNER JOIN Batch ON(Coffee.BatchID = Batch.BatchID)
    INNER JOIN Farm ON (Batch.FarmName = Farm.Name) 
    INNER JOIN InBatch ON (Batch.BatchID = InBatch.BatchID) 
    WHERE '''
     

     #Addition of criteria into sql query
    if(countries != ['']):
        for i in range(len(countries)-1):
            sql += "Farm.Country = '"+countries[i]+"' OR "
        sql += "Farm.Country = '"+countries[len(countries)-1]+"' AND "

    if(pm_incl != ['']):
        for i in range(len(pm_incl)-1):
            sql += "Batch.ProcessingMethod = '"+pm_incl[i]+"' OR "
        sql += "Batch.ProcessingMethod = '"+pm_incl[len(pm_incl)-1]+"' AND "  

    if(beans != ['']):
        for i in range(len(beans)-1):
            sql += "inBatch.BeanType = '"+beans[i]+"' OR "
        sql += "inBatch.BeanType = '"+beans[len(beans)-1]+"' AND "  

    for i in range(len(tn)-1):
        sql += "((Review.TasteNote LIKE '%"+tn[i]+"%') OR (Coffee.Description LIKE '%"+tn[i]+"%')) OR "
    sql += "((Review.TasteNote LIKE '%"+tn[len(tn)-1]+"%') OR (Coffee.Description LIKE '%"+tn[len(tn)-1]+"%'))"

    if(countries_excl != ['']):
        for i in range(len(countries_excl)-1):
            sql += " AND Farm.Country != '"+countries_excl[i]+"' "
        sql += " AND Farm.Country != '"+countries_excl[len(countries_excl)-1]+"'"

    if(beans_excl != ['']):
        for i in range(len(beans_excl)-1):
            sql += " AND inBatch.BeanType != '"+beans_excl[i]+"' "
        sql += " AND inBatch.BeanType != '"+beans_excl[len(beans_excl)-1]+"'"

    if(pm_excl != ['']):
        for i in range(len(pm_excl)-1):
            sql += " AND Batch.ProcessingMethod != '"+pm_excl[i]+"'"
        sql += " AND Batch.ProcessingMethod != '"+pm_excl[len(pm_excl)-1]+"'"  

    ##Gets list of all coffes that matches criteria
    coffees = DBHandler.DatabaseGetWihtOutInfo(sql)

    return coffees

def getCoffeByScoreAndPrice():
    """
    @returns: list of coffees sorted by highest score / price. 
    """

    sql = '''SELECT Coffee.RoasteryName, Coffee.Name, AVG(Score) as AVGScore, Price from Review NATURAL JOIN Coffee
        GROUP BY CoffeeID ORDER BY AVGScore DESC, Price ASC'''
    rows = DBHandler.DatabaseGetWihtOutInfo(sql)
    return rows