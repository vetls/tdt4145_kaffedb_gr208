import sqlite3


""" 
The function in this class populates the database.
This is done so that the functionality of the application
and database can be tested, as well as making it usable.
Given that users cannot add new Coffees, Farms, processing methods, etc.
this data has to be contained in the database before the Users can post
reviews. 
"""


def populateDatabase():
    conn = sqlite3.connect('core/coffeeDB.db')
    cur = conn.cursor()
    
    cur.execute("INSERT into User VALUES(?,?,?,?)", ("ola@gmail.com", "Ola", "Nordmann", "pass123"))
    cur.execute("INSERT into User VALUES(?,?,?,?)", ("jens@gmail.com", "Jens", "Jensen", "pass123"))
    cur.execute("INSERT into User VALUES(?,?,?,?)", ("andrea@gmail.com", "Andrea", "Andreassen", "pass123"))
    cur.execute("INSERT into Farm VALUES(?,?,?,?)", ("Farm de José", 1234, "Medellin", "columbia"))
    cur.execute("INSERT into Farm VALUES(?,?,?,?)", ("Etiopia Farm", 220, "Addis Abeba", "etiopia"))
    cur.execute("INSERT into Farm VALUES(?,?,?,?)", ("Fazenda Ambiental", 560, "Mococa", "brazil"))
    cur.execute("INSERT into Farm VALUES(?,?,?,?)", ("Nombre de Dios", 1500, "Santa Ana", "el salvador"))
    cur.execute("INSERT into ProcessingMethod VALUES(?,?)", ("washed", "mild taste"))
    cur.execute("INSERT into ProcessingMethod VALUES(?,?)", ("wet hulled", "nutty and rich taste"))
    cur.execute("INSERT into ProcessingMethod VALUES(?,?)", ("natural", "notes of syrup"))
    cur.execute("INSERT into ProcessingMethod VALUES(?,?)", ("honey processed", "rich and sweet taste"))
    cur.execute("INSERT into GrowsOnFarm VALUES(?,?)", ("Etiopia Farm", "Arabica"))
    cur.execute("INSERT into GrowsOnFarm VALUES(?,?)", ("Etiopia Farm", "Robusta"))
    cur.execute("INSERT into GrowsOnFarm VALUES(?,?)", ("Etiopia Farm", "Liberica"))
    cur.execute("INSERT into GrowsOnFarm VALUES(?,?)", ("Farm de José", "Arabica"))
    cur.execute("INSERT into GrowsOnFarm VALUES(?,?)", ("Farm de José", "Liberica"))
    cur.execute("INSERT into GrowsOnFarm VALUES(?,?)", ("Farm de José", "Excelsa"))
    cur.execute("INSERT into GrowsOnFarm VALUES(?,?)", ("Fazenda Ambiental", "Arabica"))
    cur.execute("INSERT into GrowsOnFarm VALUES(?,?)", ("Fazenda Ambiental", "Liberica"))
    cur.execute("INSERT into GrowsOnFarm VALUES(?,?)", ("Nombre de Dios", "Arabica"))
    cur.execute("INSERT into Batch VALUES(?,?,?,?,?)", (1, 2021, 15, "Farm de José", "washed"))
    cur.execute("INSERT into Batch VALUES(?,?,?,?,?)", (2, 2021, 17, "Etiopia Farm", "washed"))
    cur.execute("INSERT into Batch VALUES(?,?,?,?,?)", (3, 2021, 21, "Etiopia Farm", "honey processed"))
    cur.execute("INSERT into Batch VALUES(?,?,?,?,?)", (4, 2022, 25, "Fazenda Ambiental", "wet hulled"))
    cur.execute("INSERT into Batch VALUES(?,?,?,?,?)", (5, 2022, 8, "Nombre de Dios", "honey processed"))
    cur.execute("INSERT into InBatch Values(?,?)", ("Arabica", 1))
    cur.execute("INSERT into InBatch Values(?,?)", ("Liberica", 1))
    cur.execute("INSERT into InBatch Values(?,?)", ("Rustica", 2))
    cur.execute("INSERT into InBatch Values(?,?)", ("Excelsa", 3)) 
    cur.execute("INSERT into InBatch Values(?,?)", ("Arabica", 4))
    cur.execute("INSERT into InBatch Values(?,?)", ("Arabica", 5)) 
    cur.execute("INSERT into Coffee VALUES(?,?,?,?,?,?,?,?)", (1, "friele dark roast", 3, 2021-12-24, "Dark roasted coffee made from arabica and ", 30, "friele", 1))
    cur.execute("INSERT into Coffee VALUES(?,?,?,?,?,?,?,?)", (2, "kjeldsberg dark", 2, 2021-12-25, "Dark roasted coffee made from arabica beans", 30, "kjeldsberg", 2))
    cur.execute("INSERT into Coffee VALUES(?,?,?,?,?,?,?,?)", (3, "ali mild", 1, 2021-12-25, "mildly roasted coffee made from arabica beans", 29, "ali", 3))
    cur.execute("INSERT into Coffee VALUES(?,?,?,?,?,?,?,?)", (4, "ali chili", 2, 2022-2-25, "A spicy mix of Arabica and Excelsa beans", 29, "ali", 4))
    cur.execute("INSERT into Coffee VALUES(?,?,?,?,?,?,?,?)", (5, "vinterkaffe 2022", 1, 2022-1-20, "Lightly roasted, honey processed Bourbon with hint of berries", 60, "jacobsen & svart", 5))
    cur.execute("INSERT into Review VALUES(?,?,?,?,?,?)", (1, 1, 7, "veldig fyldig og myk. flotte nyanser med en ettersmak av mørk sjokolade", sqlite3.Date.fromtimestamp(1640682624), "andrea@gmail.com"))
    cur.execute("INSERT into Review VALUES(?,?,?,?,?,?)", (2, 3, 5, "Litt for mild. Kaffeen har flotte toner, men mangler litt karakter", sqlite3.Date.today(), "andrea@gmail.com"))
    cur.execute("INSERT into Review VALUES(?,?,?,?,?,?)", (3, 1, 7, "God og kraftig, funker konge som morgenkaffe", sqlite3.Date.today(), "ola@gmail.com"))
    cur.execute("INSERT into Review VALUES(?,?,?,?,?,?)", (4, 2, 9, "En virkelig smakfull kaffe. Floral, fyldig, og varm smak", sqlite3.Date.today(), "ola@gmail.com"))
    cur.execute("INSERT into Review VALUES(?,?,?,?,?,?)", (5, 3, 2, "Ikke noe å skrive hjem om. Tynn, smakløs og overpriset!", sqlite3.Date.today(), "ola@gmail.com"))
    cur.execute("INSERT into Review VALUES(?,?,?,?,?,?)", (6, 3, 9, "En mild og god kaffe. Ikke påtrengende. Lette hint av sitrus og sjokolade", sqlite3.Date.today(), "jens@gmail.com"))
    cur.execute("INSERT into Review VALUES(?,?,?,?,?,?)", (7, 4, 8, "Her snakker vi! sterk og kraftig chilismak. Denne får deg opp om morran!", sqlite3.Date.today(), "ola@gmail.com"))
    conn.commit()

