import sqlite3
import os
import DBsetup


"""
This file handles the set up of the database.
It sets it up using the SQL schema init.sql,
and calls DBsetUp.populateDatabase() to initially
populate the database. Paths are standardized to
support different operative systems

"""    


# Creates connection to DB and constructs cursor object
conn = sqlite3.connect('core' + os.path.sep + 'coffeeDB.db')
cur = conn.cursor()

# Opens init file and constructs database
init_file = open('core' +  os.path.sep + 'init.sql')
file_as_string = init_file.read()
cur.executescript(file_as_string)
DBsetup.populateDatabase()