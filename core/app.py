import queryHandler
import sqlite3


def main():
    print("welcome to coffee reviewer")
    Logged_in = False
    while(not Logged_in):

        action = input("type 'L' to log inn to existing user or 'R' to register new user: \n").lower()
        if action == "r":

            print("creating new user")
            first_name = input("type first name: ")
            last_name = input("type last name: ")
            email = input("type email: ")
            password = input("type password: ")
            print("\n")
            new_user = queryHandler.User(email, first_name, last_name, password)
            try:
                new_user.postUser();
            except(sqlite3.IntegrityError):
                print("Email allready in use")
            print("\nRegistration was successfull!")
            print("Log in to use the application!")
        if action == "l":
            email = input("type email: ")
            password = input("type password: ")
            user = queryHandler.logIn(email, password)
            if(user != None):
                Logged_in = True
    print("\nlogged in on", user.Email, "\n")

    while(True):
        print("Write review = 'W'\n"
        "Read reviews = 'R'\n"
        "Find all coffees = 'A'\n"
        "Find Coffee by criterias = 'F'\n"
        "Sort coffees best rating/$ = '$'\n"
        "See most active User = 'U'\n"
        "Exit = 'X'")
        action = input().lower()
        print("\n")
        if(action == "w"):
            print("writing review. Enter data")
            coffeename = input("CoffeName: ")
            roastery = input("roasteryname: ")
            tasteNote = input("reviewtext: ")
            score = input("Score: ")

            review = queryHandler.Review(user.Email, score, tasteNote, queryHandler.getCoffeeIDByNameAndRoastery(coffeename, roastery))
            review.postReview()
            print("review posted successfully\n")
        if(action == "r"):
            coffee_name = input("CoffeName: ")
            roastery = input("roasteryname: ")
            reviews = queryHandler.getReviewsForCoffee(coffee_name, roastery)
            if reviews == None:
                print("no reviews found for this coffee\n")
            else:
                
                for row in reviews:
                    print("Email: ", row[5])
                    print("TasteNote: ", row[3])
                    print("Score: ", row[2])
                    print("Date: ", row[4])
                    print("\n")
                    
            
        if(action == "a"):
            coffees = queryHandler.getAllCoffees()
            for coffee in coffees:
                print("Roastery: ", coffee[0])
                print("Name: ", coffee[1])
                print("\n")

        if(action == "f"):
            coffees = queryHandler.getCoffeeByCriteria()
            for i in coffees:
                print("Name: ", i[0], "\nRoastery: ", i[1], "\n\n")

        if(action == "u"):
            mostActive = queryHandler.getMostActiveUsersThisYear()
            for row in mostActive:
                print("Name: ", row[0], row[1])
                print("Tasted: ", row[2])
                print("\n")
                
        if(action == "$"):
            coffeesByPrice = queryHandler.getCoffeByScoreAndPrice()
            for coffee in coffeesByPrice:
                print("Roastery: ", coffee[0])
                print("Name: ", coffee[1])
                print("Average Score: ", round(coffee[2], 2))
                print("Price: ", coffee[3], "\n")
        
        if(action == "x"):
            print("\n goodbye")
            break
                
            
if __name__ == '__main__':
    main()