# TDT4145_KaffeDB_Gr208

KaffeDB prosject in TDT4145 Spring 2022

### Oversikt over innhold i CoffeeDB

# Filer:
- init.sql: SQL skjema for å konstruere databasen, brukes av init.py.
- Init.py: Bruker SQL skjemaet init.sql til å konstruere en databasen coffeeDB.db og populere den med data
- app.py: main metode som styrer hvordan programmet kjøres  
- queryHandler.py: pythonfil som inneholder SQL spørringer for å hente ut forskjellig informasjon fra databasen
- DBhandler.py: pythonfil som utfører spørringene spesifisert i queryHandler
- coffeDB.db: Selve databasefilen